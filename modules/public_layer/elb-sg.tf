#==============================================================
# Public / elb-sg.tf
#==============================================================

# Security groups for Elastic Load Balancer

resource "aws_security_group" "elb_sg" {
  # Allow HTTP from anywhere
  name = "elb_sg"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    # Enable different access for dev / test / prod
    cidr_blocks = [var.http_ip_address[var.environment]]
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    # Enable different access for dev / test / prod
    cidr_blocks = [var.http_ip_address[var.environment]]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = var.vpc_id

  tags = {
    Name        = "${var.stack_name}-${var.environment}-elb-http-inbound"
    owner       = var.owner
    stack_name  = var.stack_name
    environment = var.environment
    created_by  = "terraform"
  }
}
