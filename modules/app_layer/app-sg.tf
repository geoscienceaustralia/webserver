#==============================================================
# App / app-sg.tf
#==============================================================

# Security group for the application server

resource "aws_security_group" "app_sg" {
  # Allow HTTP from within VPC
  name   = "app_server"
  vpc_id = var.vpc_id

  #--------------------------------------------------------------
  # HTTP - IN
  #--------------------------------------------------------------

  # Only allow HTTP from the ELB

  ingress {
    from_port       = var.port_num
    to_port         = var.port_num
    protocol        = "tcp"
    security_groups = [var.elb_sg_id]
  }

  #--------------------------------------------------------------
  # ANY - OUT
  #--------------------------------------------------------------


  # Allow any access out

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #--------------------------------------------------------------
  # Tags
  #--------------------------------------------------------------

  tags = {
    Name        = "${var.stack_name}-${var.environment}-app-server"
    owner       = var.owner
    stack_name  = var.stack_name
    environment = var.environment
    created_by  = "terraform"
  }
}
