#==============================================================
# App / outputs.tf
#==============================================================

output "asg_id" {
  value = aws_autoscaling_group.asg.id
}

output "webapp_lc_id" {
  value = aws_launch_configuration.lc.id
}

output "webapp_lc_name" {
  value = aws_launch_configuration.lc.name
}
