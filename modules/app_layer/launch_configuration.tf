#==============================================================
# App / launch-configuration.tf
#==============================================================

# Application server

data "template_file" "userdata" {
  # Treat the userdata like a template so we can load TF variables into it
  template = file(var.userdata_filepath)
}

resource "aws_launch_configuration" "lc" {
  lifecycle {
    create_before_destroy = true
  }

  image_id      = var.asg_amis
  instance_type = var.instance_type

  security_groups = [aws_security_group.app_sg.id]

  user_data                   = data.template_file.userdata.rendered
  key_name                    = var.key_name
  associate_public_ip_address = false
}
