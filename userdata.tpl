#!/bin/bash

#==============================================================
# userdata.tpl
#==============================================================

# This is a template that is used to generate the userdata.sh
# that will be run on the application server when it is built.
# The variables are replaced during a terraform apply.

#--------------------------------------------------------------
# Install Packages
#--------------------------------------------------------------

# In a production system this would be baked into the AMI, 
# Look into our packer examples to see how this can be done.

# Install Apache (webserver) 
sudo apt-get -y update
sudo apt-get install -y apache2

#--------------------------------------------------------------
# Configure Webpage
#--------------------------------------------------------------

# Create a simple web page to prove apache works

echo "<h1>Hello World</h1>" | sudo tee -a /var/www/html/index.html